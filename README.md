[![build status](https://gitlab.com/nabsha/resume/badges/master/build.svg)](https://gitlab.com/nabsha/resume/commits/master)

# Migrated to https://github.com/nabsha/resume.git

My resume based on https://jsonresume.org/

Deployed at
http://ns-resume.herokuapp.com/

