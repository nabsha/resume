Hi, 

I would like to introduce myself as software engineer and architect with around 10+ years of experience of software development and design. In last 7 years, I have been engaged in enterprise java application development as well as systems analyst. Before that, during 2007-9, I was working as embedded systems engineer, implementing signal processing algorithms. And before that I was part of academia, researching and assisting in Electrical Engineering Dept, CIIT, for Computer Architecture, OS and Distributed Computing.

During last 7 years, I have been building software for enterprise integration using JMS(like ActiveMQ and IBM Websphere MQ), Integration DSLs(like Apache Camel Spring Integration) and Application containers (like Glassfish and JBoss Fuse). In addition to that, I am keenly interested in JMX technologies and maintain Java container monitoring and application optimizations practices.

I sincerely hope the given role closely matches my profile and I am sure to attract your consideration for it. 

Regards,
Nabeel
0416648428